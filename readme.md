### gitlab ci

* 安装gitlab-ci-multi-runner
    ```bash
    # 添加yum源 
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | sudo bash
    # 安装 
    yum install gitlab-ci-multi-runner
    ```

* runner注册
    ```bash
    gitlab-ci-multi-runner register
    # 安装服务
    gitlab-ci-multi-runner install -u root
    // 开启服务
    gitlab-ci-multi-runner start
    # 停止服务
    gitlab-ci-multi-runner stop
    ```

* 注册过程 
    ![runner_register.png](assets/runner_register.png)
    > 私有的runner注册时会需要提供项目的url和token，在项目的流水线中可以找到


* 示例
    ```yaml
    # cache这里必须要加，不然在执行job时，会清掉之前的文件
    # 也就是 build_image时会先清掉build_exec这一步产生的文件，后续也是一样
    cache:
    paths:
        - ./

    stages:
    - build_exec
    - build_image
    - push_image

    job_build_exec:
    stage: build_exec
    script:
        - go build .
    only:
        - master
    # tags可以用来选择runner，runner注册时也会标记一些tag
    tags:
        - autobuild

    job_build_image:
    stage: build_image
    script:
        - docker build -t 122.11.58.163:5000/testci .
    tags:
        - autobuild

    job_push_image:
    stage: push_image
    script:
        - docker push 122.11.58.163:5000/testci
    tags:
        - autobuild

    job_deploy:
    # 这个任务只在日程中生效，普通的push是不会触发的
    only:
        - schedules
    # except和only相反
    # except:
    #   - schedules
    stage: deploy
    script:
        - echo "aaaaa"
    environment:
        name: k8s
    tags:
        - autobuild        
    ```
    > 这里提供了三个阶段的job，分别是生成可执行文件，生成镜像，push镜像，是顺序执行的，同一个stage的job是并行执行的

* gitlab-ci触发器

    - 创建触发器
    ![create_trigger.png](assets/create_trigger.png)

    - web钩子，参考触发器下方的web钩子说明，拼好url，post一下即可触发流水线
    ![post_url.png](assets/post_url.png)

* gitlab k8s集成    

    - 创建服务账号
        ```bash
        kubectl apply -f http://x.co/rm082018
        ```
    - 获取服务token
        ```bash
        kubectl describe secrets/gitlab-secret -n gitlab-managed-apps | grep token
        ```

    - CA Certificate
        > 从kube config文件复制出来，并在base64解码后再使用

    - **没搞定，一直不清楚k8s为什么报remote error: tls: internal error**
    

* 日程表
    - 可以添加日程事件，触发流水线操作，添加很简单，不用文档了
    - 通过如下可以限制只有日程生效的job
        ```yaml
        job_abc:
        only:
          - schedules
        # except 排除
        except:
          - schedules
        ```

* demo操作
    ```
    - runer1, 日常push时触发，会执行以下job：
        1. 编译生成可执行文件
        2. 将可执行文件生成docker镜像
        3. 将镜像push到仓库
    - runer2, 部署在另一台机器，由日程定时触发，可以更新发布

    以这样的流程实现开发环境服务的更新
    ```

* 镜像仓库